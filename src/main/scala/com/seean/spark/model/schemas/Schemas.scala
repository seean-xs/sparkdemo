package com.seean.spark.model.schemas

import org.apache.spark.sql.types._

/**
  *
  * 所有表对应的样式
  * @authur wsx
  * @date 2020/6/24 15:26
  */
object Schemas {

  val userSchema = StructType(
      StructField("id", DecimalType(10,0)) ::
      StructField("name", StringType) ::
      StructField("point", DecimalType(11,2)) ::
      StructField("age", DecimalType(10,0)) ::
      StructField("birth", TimestampType) ::
      Nil)
  val demoSchema1 = StructType(
      StructField("id", IntegerType) ::
      StructField("name", StringType) ::
      StructField("age", IntegerType) ::
      StructField("point", DecimalType(11,2)) ::
      StructField("birth", TimestampType) ::
      Nil)
}
