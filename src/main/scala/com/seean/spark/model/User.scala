package com.seean.spark.model

import java.sql.Timestamp

import com.seean.spark.model.base.BaseModel

/**
  *
  * @authur wsx
  * @date 2020/6/24 15:26
  */
class User extends Serializable with BaseModel{
  // @BeanProperty get/set method
  var id:BigDecimal=0
  var name:String=""
  var point:BigDecimal = 0.0
  var age:BigDecimal=0
  var birth:Timestamp=_

  override def toList():List[Any] = {
    List(id,name,point,point,birth)
  }
}
