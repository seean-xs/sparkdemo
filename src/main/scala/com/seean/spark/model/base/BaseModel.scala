package com.seean.spark.model.base

/**
  * toList
  * @authur wsx
  * @date 2020/6/24 15:26
  */
trait BaseModel {
  def toList():List[Any] = List()
}
