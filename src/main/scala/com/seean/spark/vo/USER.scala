package com.seean.spark.vo

import java.sql.Timestamp

/**
  * Models
  *
  * @authur wsx
  * @date 2020/6/24 15:26
  */
case class USER(

                 ID: BigDecimal,
                 NAME: String,
                 POINT: BigDecimal,
                 AGE: BigDecimal,
                 BIRTH: Timestamp
                 //List: mutable.ArrayBuffer[String]
               )
