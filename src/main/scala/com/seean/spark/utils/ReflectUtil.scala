package com.seean.spark.utils

import scala.reflect.runtime.{universe => ru}

/**
  *  反射工具类
  * @authur wsx
  * @date 2020/6/24 15:26
  */
object ReflectUtil {

  def main(args: Array[String]): Unit = {

    val classMirror = ru.runtimeMirror(getClass.getClassLoader)         //获取运行时类镜像
    val classTest = classMirror.staticModule("com.seean.spark.model")          //获取需要反射object
    val methods = classMirror.reflectModule(classTest)                  //构造获取方式的对象
    val objMirror = classMirror.reflect(methods.instance)               //反射结果赋予对象
    val method = methods.symbol.typeSignature.member(ru.TermName("say")).asMethod  //反射调用函数
    val result = try
      objMirror.reflectMethod(method)("ref")
    catch {
      case e: ScalaReflectionException => {
      }
    }
    //最后带参数,执行这个反射调用的函数
    val r = result.asInstanceOf[String]
    println("-----"+r)
  }

}
