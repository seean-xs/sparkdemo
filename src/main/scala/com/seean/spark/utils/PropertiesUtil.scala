package com.seean.spark.utils

import java.io.FileInputStream
import java.util.Properties

/**
  *
  * @authur wsx
  * @date 2020/6/28 11:42
  */
object PropertiesUtil {

  def loadProperties():Properties = {
    val properties = new Properties()
    val path = Thread.currentThread().getContextClassLoader.getResource("config.properties").getPath //文件要放到resource文件夹下
    properties.load(new FileInputStream(path))
    properties
  }

}
