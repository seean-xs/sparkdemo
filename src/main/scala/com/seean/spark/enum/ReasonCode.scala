package com.seean.spark.enum

/**
  *
  * @authur wsx
  * @date 2020/6/24 16:36
  */
object ReasonCode extends Enumeration {
  type reasonCode = Value //声明枚举对外暴露的变量类型
  val Code_1 = Value("1")
  val Code_2 = Value("2")
  val Code_3 = Value("3")
  val Code_4 = Value("4")
  val Code_5 = Value("5")
  val Code_6 = Value("6")
}
